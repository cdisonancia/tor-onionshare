# Límites y consideraciones

**1- Velocidad**
Respecto del uso del navegador Tor, es necesario señalar que, al pasar por varias capas de cifrado, el tiempo de acceso a los sitios, incluso en aquellos que no son de tipo onion, es más lento en relación con el uso de un navegador normal. El proceso de cifrado y descifrado que realizan los nodos y el navegador lleva más tiempo para poder mantener las capas de protección que aseguran el anonimato.

**2- Tamaño de ventana**
Otro elemento a destacar es que al abrirse la ventana del navegador, esta tendrá un tamaño mucho menor al de nuestra pantalla. El objetivo de esto es que todos los usuarios que estén usando Tor tengan la misma configuración de tamaño del navegador, reduciendo el riesgo de identificación por el tamaño de su pantalla. Esto se debe a que los navegadores, cada vez que un navegador se conecta a un sitio, entregan información del dispositivo, como el tamaño de la ventana en que se ejecuta. 

**3- No abrir documentos mientras se está conectado a Tor**
Algunos formatos de archivos que descargamos y abrimos, como PDFs, pueden contener internamente funciones o elementos que requieren hacer una consulta en internet, lo que podría revelar la ubicación real que tenemos.

**4- Usar navegador predefinido de Tor**
Los usuarios avanzados, por comodidad, podrían configurar un navegador convencional para que use los puertos de Tor, permitiendo así navegar por la red onion con un programa más agradable que el ofrecido por el proyecto Tor. Sin embargo, esto no es recomendable, ya que el navegador Tor está configurado para reforzar el anonimato al restringir de manera predeterminada algunos componentes o facilitando el reinicio de los nodos conectados. Si navegamos por la red onion, es preferible hacerlo desde este navegador. Además, no es recomendable instalar nuevos complementos en él porque aumentan el riesgo de ser identificado.

**5- Discreción al visitar sitios de la "red clara" desde Tor** (solo visitar sitios con "HTTPS")
Cuando se usa Tor para visitar sitios convencionales, no onion, nuestra navegación debe salir por el "nodo de salida" de la red para dirigirse al destino. En esta salida ocurren dos hechos: por un lado, el tráfico que sale de Tor deja de estar cifrado por lo que se transforma en tráfico normal; y por otro lado, los nodos de salida son identificables por lo que el sitio de destino sabe que hay una conexión desde Tor, aunque, desde luego, no puede identificar al usuario. 

Esto significa tomar dos precauciones: visitar sitios que tengan en su url el "https" que indica que cuenta con cifrado del tráfico propio y, además, ser cautos con nuestras cuentas y comportamiento. Si entramos con una cuenta que ya nos identifica, perdemos el anonimato, así como también si entregamos información que nos haga identificables. 

**6- No usar Redes Sociales corporativas**
El uso de redes sociales elimina inmediatamente el anonimato que existe al estar en la red Tor, ya sea por ingresar con nuestra cuenta personal －previamente identificada－ o porque este tipo de redes corporativas pueden rastrear nuestro comportamiento en el navegador una vez iniciada la sesión, entre otros medios que utilizan. De hecho, Facebook y Twitter cuentan con dominios onion, por lo que es posible conectarse desde Tor, pero requiere contar de todos modos  con una cuenta verificada, por lo que dependerá de la estrategia de cada organización evaluar el riesgo que esto supone en relación a su actividad.

**7 - Usa puentes en caso de censura de Tor**
Cuando nos conectamos a la red Tor, el proveedor de internet puede ver que estamos en dicha red, aunque no puede conocer qué hacemos en ella ni qué sitios visitamos. En algunos países, el acceso a Tor se encuentra restringido o es ilegal, por lo cual es necesario acceder de una manera no convencional. Para esto existen los "puentes" que, en el proyecto Tor, son nodos especiales o repetidores que permiten conectar al usuario con algún nodo de entrada a la red a través de un nodo intermediario, que no se encuentra registrado como perteneciente a la red, haciendo más difícil identificar cuándo un usuario usa Tor. 

Si esta funcionalidad es necesaria, debemos dirigirnos a los ajustes o configuraciones del navegador y elegir la opción "Tor" y activar el tipo de puente que queramos. El detalle de los tipos de puentes se encuentra [aquí](https://tb-manual.torproject.org/circumvention/).

![tor19](../../img/IMG-TorBrowser/tor19.png)
