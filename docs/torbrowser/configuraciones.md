# Configuraciones

En la esquina superior derecha del navegador hay dos íconos, el primero permite definir el nivel de seguridad y el segundo reiniciar la ubicación de la red Tor.

![tor10](../../img/IMG-TorBrowser/tor10.png)

Si se presiona el primero, se despliega la notificación que nos avisa que podemos cambiar la configuración de seguridad, por defecto en "*estándar*", que es el tipo de navegación sugerida si no se quiere perder ninguna característica habitual de los sitios que visitamos, sobre todo sitios confiables. En caso de querer elevar el nivel de seguridad, se debe presionar "*Cambiar*".

![tor11](../../img/IMG-TorBrowser/tor11.png)

Existen tres niveles de seguridad, el que viene por defecto es el ""**Estándar**"" que cuenta con todas las funcionalidades del navegador y de los sitios visitados. Es la configuración más adecuada para una navegación ocasional y que no necesita ingresar a sitios peligrosos o agresivos con la privacidad. El segundo nivel es "**Más seguro**" y deshabilita características que son a menudo peligrosas como videos, JavaScript en sitios sin https y otras funcionalidades. El nivel más elevado es "**El más seguro de todos**" que restringe todas las funcionalidades de los sitos visitados que no sean elementos esenciales, por lo que la mayoría de medios (imágenes y programas de segundo plano como JavaScript) no aparecerán; nivel recomendado cuando se requiere reducir el riesgo al mínimo. **A mayor seguridad, menos funcionalidades disponibles en los sitios web**, por lo que depende del usuario definir cuánto riesgo es preferible correr en cada caso.

![tor12](../../img/IMG-TorBrowser/tor12.png)

Si, en cambio, presionamos el segundo de los íconos, se desplegará una ventana consultando si queremos reiniciar el navegador para que nuestra identidad en la red, la IP de salida de Tor, cambie a una nueva ubicación. Para esto presionamos "**Sí**". 

![tor15](../../img/IMG-TorBrowser/tor15.png)