 
# Qué es Tor

Al navegar por Internet, los sitios que visitamos, los contenidos que intercambiamos y las huellas que dejamos de nuestro comportamiento en línea están bajo control de una infraestructura tecnológica que no manejamos. Los proveedores de internet, que administran nuestra conexión, y las corporaciones propietarias de los cables submarinos y los centros de datos, por donde viaja y se almacena nuestra información, articulan la imperceptible red privada de empresas que controla lo que llamamos internet. Nuestras comunicaciones están atravesadas por la voracidad del mercado y por la vigilancia corporativa y estatal

Esta red de intermediarios, que no podemos ver cuando hacemos uso de nuestros dispositivos, cuenta con una historia de acumulación y apropiación que explica cómo [las comunicaciones de toda la sociedad se han convertido en un apéndice más del capitalismo](https://colectivodisonancia.net/autonomia/redes-p2p/#armas-y-cables). Esta situación es un escenario adverso para comunicarnos y organizar nuestras luchas, puesto que significa que enfrentamos permanentemente una situación vigilancia comercial y política que ha quedado en evidencia [en reiteradas ocasiones](https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data).

Mientras la infraestructura tecnológica dependa de empresas y Estados, solo un horizonte de autonomía tecnológica comunitaria puede evitar que las redes de comunicación sean plataformas de vigilancia y mercantilización. No obstante, para avanzar colectivamente en las luchas en las que participamos, existen actualmente maneras de enfrentar esta situación haciendo uso de la colaboración de organizaciones y de las herramientas criptográficas de navegación segura y anonimato como es el caso de la red Tor.

Tor es el acrónimo de ***The Onion Router*** －enrutado o direccionamiento cebolla, en español－ y hace referencia a un conjunto de elementos que existen para mantener en funcionamiento una red de navegación global, segura y anónima:

* La "red Tor" es el sistema de conexiones entre dispositivos que permiten el funcionamiento de una navegación anónima.

* El Navegador "Tor" es el que se emplea para poder conectarse a la red Tor.

* El "Proyecto Tor" es la organización que mantiene el desarrollo de esta red y su navegador.

En la década de los 90, el Laboratorio de investigación naval de EEUU (NRL) exploró la posibilidad de establecer una navegación anónima usando el internet ya existente. En la siguiente década, ya separado de la investigación militar, aparece el proyecto Tor con su propuesta de red basada en un sistema de comunicación que garantiza un tipo de comunicación digital que se distribuye en una red anónima por medio de varias capas de cifrado que se superponen, como si fuecen las capas de una cebolla －onion, en inglés－, metáfora que da nombre al proyecto.