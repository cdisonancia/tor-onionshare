# Por qué usar Tor

Además de la ventaja que supone navegar de manera anónima; para la lucha de las organizaciones anticapitalistas, es importante aprender a utilizar este procedimiento por varios motivos:

- Evitar la vigilancia masiva de empresas y corporaciones

- Sortear la censura hacia sitios y contenidos que imponen algunos Estados

- Proteger la comunicación e información interna de nuestra organización

- Eludir la persecución digital focalizada de la cual podamos ser objeto, particularmente cuando tenemos conocimiento de seguimiento previo

- Publicar información o denuncias de manera anónima (filtración). 
