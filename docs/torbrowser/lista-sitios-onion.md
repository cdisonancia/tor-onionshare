
# Lista de sitios onion recomendados

* El buscador DuckDuckGo: https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/ | [Versión clara](https://duckduckgo.com/)
* La biblioteca Z-library http://loginzlib2vrak5zzpcocc3ouizykn6k5qecgj2tzlnab5wcbqhembyd.onion/ | [Versión clara](https://es.z-lib.org/)
* La plataforma de pastebin, －o de almacenamiento de textos públicos－ ZeroBin http://zerobinftagjpeeebbvyzjcqyjpmjvynj5qlexwyxe7l3vqejxnqv5qd.onion  [Versión clara](https://zerobin.net/ )
* El buscador de sitios onion Ahmia: http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/ | [Versión clara](https://ahmia.fi/)