# Navegador Tor

El Navegador Tor, o *Tor Browser*, es un navegador web basado en Firefox que ha sido adaptado para mantener de la mejor manera posible el anonimato de la red Tor. Además de su instalación, no requiere de ninguna configuración particular para conectarse a la red Tor, exceptuando algunas configuraciones que son opcionales. 

## Instalación

Para descargar el navegador, debemos dirigirnos a [https://www.torproject.org/es/download/](https://www.torproject.org/es/download/), donde tenemos que seleccionar el sistema operativo que estamos usando y presionar **Descarga**.

![tor01](../../img/IMG-TorBrowser/tor01.png)

Una vez descargado el archivo, tenemos que ir a la ubicación en donde se encuentra el nuevo archivo y ejecutarlo para iniciar la instalación. －En el caso de sistemas GNU/Linux, se trata de un archivo tar.xz que solo requiere ser descomprimido y para luego ejecutar el archivo **start-tor-browser.desktop**－.

![tor02](https://gitlab.com/krv00/guia-tor/-/raw/main/docs/img/IMG-TorBrowser/tor02.png)

Una vez ejecutado el programa, se debe aceptar el realizar modificaciones en el dispositivo, es decir, aceptar instalar Tor; para luego seleccionar el lenguaje del instalador.

![tor03](../../img/IMG-TorBrowser/tor03.png)
![tor04](../../img/IMG-TorBrowser/tor04.png)

Por defecto, el programa elige el escritorio del usuario para la ubicación de instalación, destino que puede ser modificado, si así se desea, en **Examinar**. Una vez aceptada la ubicación, debemos presionar **Instalar**.

![tor05](../../img/IMG-TorBrowser/tor05.png)

Tras finalizar la instalación, presionamos **Terminar** para iniciar el navegador, el cual debe contar ya con un ícono en el escritorio.

![tor06](../../img/IMG-TorBrowser/tor06.png)
![tor07](../../img/IMG-TorBrowser/tor07.png) 
