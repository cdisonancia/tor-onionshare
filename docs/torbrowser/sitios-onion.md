 
# Sitios Onion

Los **sitios onion** son sitios web creados dentro de la red Tor y solo son accesibles desde ahí, es decir, no pueden ser accedidos sin usar el navegador Tor o algún programa que se conecte a esta red. La ventaja de estos sitios, tanto para los usuarios como para quienes mantienen estas instancias, es el anonimato mutuo puesto que ni el visitante ni el anfitrión pueden conocer la ubicación real del otro. Del mismo modo, el anonimato de estos sitios los hace fuertemente resistentes a la censura debido a que, al no exponer su lugar de conexión real, no se les puede exigir que se desconecten².  

El anonimato y la resistencia a la censura que tienen los sitios onion han recibido una atención sensacionalista que ha llevado a señalar la red Tor como una instancia exclusiva de actividad delictiva y a llamarla, con un velo de misterio,"deep web". Si bien el conjunto de sitios accesibles únicamente desde Tor podrían ser identificados, metafóricamente hablando, como una "red profunda" en relación con los sitios convencionales －o red clara－, es más idóneo referirse a ellos por el nombre de la propia red que permite su acceso －red Tor o sitios onion－ para evitar el carácter peyorativo que tiene la expresión "deep web" en la mayoría de los casos en la prensa empresarial.

Los sitios onion tienen una estructura como esta, una serie de caracteres que finalizan en **.onion** como en esta url de la versión onion de DuckDuckGo

```
https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/
```

Al entrar al sitio, a este o a cualquier otro, se puede presionar el ícono de Tor para verificar los nodos de la red, donde veremos los primeros tres por los que pasa nuestro navegador, sumado a los otros tres por los cuales se conecta el sitio. Es decir, al conectarnos a un sitio onion, la conexión mutua posee seis capas de cifrado, anonimizando nuestro tráfico y también la ubicación del servidor que mantiene el sitio.

![tor16](../../img/IMG-TorBrowser/tor16.png)

----
2: Tor no es el único proyecto que puede establecer una navegación anónima o una red interna independiente del internet habitual. En el caso de querer conocer otras herramientas de este tipo, destacan [I2P](https://geti2p.net/es/) y [Freenet](https://freenetproject.org/), esta última es una red descentralizada y, en principio, aislada del internet convencional.