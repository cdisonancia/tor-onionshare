 
# Dispositivos móviles

También es posible descargar e instalar el navegador Tor para [Android desde F-Droid o Play Store](https://www.torproject.org/download/#android). En iOS no está disponible aún, pero existe una aplicación alternativa llamada [onion Browser](https://onionbrowser.com/).