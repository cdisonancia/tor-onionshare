# Uso del navegador
Al abrir por primera vez el navegador Tor, aparecerá un mensaje indicando que se puede solicitar la versión en inglés de los sitios que visitaremos y mejorar con ello nuestro anonimato. Si no es un problema para la consulta de contenidos, es preferible indicar **Sí** y navegar por los sitios en su versión en inglés, cuando estén disponible. Si preferimos los textos en español para no dificultar el acceso a los contenidos, debemos elegir la opción **No**.

![tor08](../../img/IMG-TorBrowser/tor08.png)

Luego veremos la opción de conectar automáticamente el navegador a la red Tor o elegir la configuración manual cada vez que se abre la sesión. En la mayoría de los casos, solo es necesaria la conexión automática, para lo cual hay que marcar la casilla y elegir **Conectar**.

![tor09](../../img/IMG-TorBrowser/tor09.png)

Una vez realizada la conexión del navegador a la red Tor, que incluye conectarse a los tres nodos e iniciar el proceso de cifrado, el navegador se encuentra listo para utilizarse de manera anónima para visitar sitios, ya sea usando la barra del navegador o usando el buscado Duck Duck Go.

![tor18](../../img/IMG-TorBrowser/tor18.png)

Para verificar cuál es nuestra dirección IP －es decir, nuestra ubicación en internet con este navegador－ podemos ir a [https://browserleaks.com/ip](https://browserleaks.com/ip) donde se verifica la IP con la cual navegamos, que en este caso corresponde a un nodo de salida de Tor. En cada sesión, la dirección será distinta, de este modo, los sitios que visitamos no pueden saber nuestra ubicación real.

![tor17](../../img/IMG-TorBrowser/tor17.png) 
