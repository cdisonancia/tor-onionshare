# Apoyemos el Proyecto Tor

El Proyecto Tor se mantiene principalmente por medio de donaciones de organizaciones y usuarios. Por este motivo, es recomendable, siempre que podamos, contribuir con alguna donación que se puede realizar a través de su sitio web: https://donate.torproject.org/ 
