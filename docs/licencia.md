# Licencia

![CC BY-NC-SA][(https://colectivodisonancia.net/wp-content/uploads/2020/02/CC-BY-NC-SA_90.jpg)

Esta guía, elaborada por **Colectivo Disonancia**, está bajo una licencia [Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es), exceptuando los contenidos que indiquen lo contrario.
