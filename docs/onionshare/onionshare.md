# OnionShare 

OnionShare es un programa que, haciendo uso de la red Tor, permite intercambiar archivos, utilizar una sala de chat y levantar un sitio web en la red onion de manera sencilla. Por ende, es una herramienta indispensable para la comunicación, intercambio y publicación de contenidos de manera anónima y efímera. Estas funcionalidades **solo estarán disponibles mientras el programa esté encendido**; una vez cerremos OnionShare, el intercambio, chat y sitio que hayamos establecido desaparecerá, generando urls onion distintas en cada sesión en que usemos el programa. 

Para utilizarlo, además de instalarlo, nuestros contactos deben tener instalado previamente el navegador Tor, revisado en esta guía.

Esta herramienta ha sido desarrollada por [Micah Lee](https://micahflee.com/), analista de tecnología de la información y director de Infosec en [The Intercept](https://theintercept.com/staff/micah-lee/), junto a otros desarrolladores. Su [código es público](https://github.com/onionshare/onionshare) y se encuentra bajo una licencia de Software Libre. 
