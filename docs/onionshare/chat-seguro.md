# Chat seguro

Otra opción de OnionShare es el chat, que permite a dos o más interlocutores participar de un chat protegido por la seguridad de la red Tor y del cual no quedará registro una vez termine. Para acceder a esta función, debemos elegir la opción "**Empezar a charlar**".

![onionshare23](../../img/IMG-OnionShare/onionshare23.png)

La principal opción a configurar es si el enlace del chat tendrá acceso público o no. Para esto, si seleccionamos la casilla destacada, bastará con tener el enlace para poder acceder; si no la marcamos, se requerirá una contraseña que generará OnionShare. Además, en la "**configuración avanzada**" podemos determinar un intervalo horario para dejar activo el chat si queremos poner ese tipo de restricción.

Una vez seleccionadas las opciones que elegimos, podemos empezar presionando "**Iniciar servidor de chat**".

![onionshare24](../../img/IMG-OnionShare/onionshare24.png)

OnionShare nos mostrará la url que debemos compartir con nuestros interlocutores para que puedan acceder al chat en sus navegadores Tor, acción que también debemos hacer nosotros en nuestro navegador. Si dejamos el chat como no público, aparecerá también la contraseña que debemos compartir para poder acceder.

También está disponible el botón "**Detener servidor de chat**" para detener la comunicación.

![onionshare25](../../img/IMG-OnionShare/onionshare25.png)

Abrimos el navegador Tor e ingresamos la misma url que le compartimos a nuestros contactos y veremos la interfaz del chat. Elegimos nuestro nombre y ya podemos empezar a conversar de manera segura.

![onionshare26](../../img/IMG-OnionShare/onionshare26.png) 
