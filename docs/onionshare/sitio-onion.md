# Sitio onion

Por último, OnionShare también cuenta con una función para habilitar un sitio web onion, en el cual podamos publicar contenidos en la red Tor, contenido que será resistente a la censura y, si el enlace del sitio se comparte sin estar vinculado a nuestra identidad, de modo anónimo.

Para esto, debemos elegir "Alojar un sitio web" presionando **Empezar a alojar**.

![onionshare20](../../img/IMG-OnionShare/onionshare20.png)

Aparecerá la interfaz para que agreguemos o soltemos ahí los archivos necesarios que compongan nuestro sitio web onion. Ahora bien, elaborar un sitio web requiere de ciertos conocimientos en programación o manejo de lenguajes de marcado, como HTML. Sin embargo, un sitio web con características básicas no es difícil de montar. 

Para este ejemplo se usó un archivo html que tiene 10 líneas y solo contiene texto para mostrar. Se puede descargar como archivo comprimido [aquí](https://colectivodisonancia.net/wp-content/uploads/2022/05/html-prueba-sitio-onion.zip).

![onionshare28](../../img/IMG-OnionShare/onionshare28.png)

Cuando hayamos agregado los archivos necesarios, aparecerán las opciones de configuración del sitio, donde la principal es la casilla que permite a nuestro sitio ser de acceso público, es decir, no requerir contraseña. Si no lo marcamos, OnionShare generará una contraseña necesaria.

Cuando tengamos todo listo, presionamos "**Comenzar a compartir**".

![onionshare29](../../img/IMG-OnionShare/onionshare29.png)

Aparecerá el enlace que conduce a nuestro recién publicado sitio onion －y contraseña en caso de que no dejáramos el enlace en modo público－. Ahora debemos copiar la url .onion y compartirla para que pueda ser visitada desde el navegador Tor. Además, al presionar el botón "**Dejar de compartir**" haremos que nuestro sitio deje de estar disponible en la red.

![onionshare21](../../img/IMG-OnionShare/onionshare21.png)

Al visitar el sitio desde el navegador Tor podemos ver el contenido. 

Explorando y aprendiendo algunos elementos básicos de html podemos elaborar un sitio con mayores funciones, como disponer de textos con enlaces, los cuales incluso pueden ser los mismos enlaces de archivos compartidos que generamos con OnionShare.

![onionshare22](../../img/IMG-OnionShare/onionshare22.png)

No está de más recordar que quienes visiten nuestro sitio no tienen forma de saber dónde está alojado realmente el sitio, así como tampoco los visitantes del sitio pueden ser identificados en su ubicación real. De este modo, se emplea un medio de comunicación seguro y resistente a la censura. 
