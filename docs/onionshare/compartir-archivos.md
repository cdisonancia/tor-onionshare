# Compartir archivos

Cuando el programa se abre, podemos ver su interfaz y las 4 opciones que ofrece. La primer es "**Compartir archivos** que sirve para poder generar una url de tipo onion que permitirá a nuestros destinatarios descargar los archivos que hayamos seleccionado desde nuestro dispositivo. Para iniciar el proceso, hacemos click en "**Empezar a compartir**".

![onionshare06](../../img/IMG-OnionShare/onionshare06.png)

Aparecerá la opción para compartir archivos, lo cual se puede hacer arrastrando un archivo hasta la ventana y luego soltarlo, así como también presionando "**Añadir**" en la esquina inferior derecha para buscar el archivo en nuestro dispositivo.

![onionshare07](../../img/IMG-OnionShare/onionshare07.png)

Cuando agregamos uno o varios archivos, se verán las opciones de configuración, donde destacan 3. La primera es una opción que limita los archivos compartidos a una sola descarga, de manera que una vez que nuestro destinatario tenga los archivos que les compartimos, OnionShare desconectará el intercambio.

La segunda opción permite reabrir esta pestaña de archivos compartidos cuando encendamos nuevamente el programa. El objetivo es facilitarnos el compartir un mismo archivo en más de una ocasión.

Y la tercera opción permite dejar el enlace que compartimos como accesible públicamente. Por defecto, cuando se genere el enlace, OnionShare también generará una contraseña －o llave privada－ que restringe el acceso al enlace de descarga solo a los que tienen esta contraseña. Al marcar esta opción, dicha verificación no es necesaria.

Además, no es recomendable agregar un título personalizado al enlace, ya que puede incluir información que ponga en riesgo nuestro anonimato al compartir el enlace

Cuando estemos preparados para compartir, presionamos **Comienza a compartir**

![onionshare08](../../img/IMG-OnionShare/onionshare08.png)

El Archivo ya se estará compartiendo y para poder darle acceso a nuestros contactos o a quien queramos, debemos copiar la URL que termina en **.onion**. Este enlace solo es accesible desde el navegador Tor, por lo cual, tanto nosotros como nuestro destinatario, podemos mantener el anonimato de nuestra ubicación real.

Si no seleccionamos la opción de enlace público, también aparecerá la contraseña que debemos compartir a nuestro interlocutor para que pueda acceder al enlace. Si en cambio marcamos esa opción, no es necesario.

![onionshare09](../../img/IMG-OnionShare/onionshare09.png)

Quien recibe el enlace debe abrirlo desde su navegador Tor para ver los archivos que se pueden descargar －y se le pedirá la contraseña en caso de que así haya quedado configurado－.

![onionshare10](../../img/IMG-OnionShare/onionshare10.png)
![onionshare11](../../img/IMG-OnionShare/onionshare11.png) 
