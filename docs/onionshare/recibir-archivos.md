# Recibir archivos

En el caso de que alguien nos quiera enviar un archivo por la red Tor pero no cuenta con OnionShare, podemos usar la opción de **Recibir archivos** para crear un enlace con el cual nuestro interlocutor puede subir un archivo y nosotros recibirlo desde OnionShare.

![onionshare14](../../img/IMG-OnionShare/onionshare14.png)

Al entrar en esta opción, podemos seleccionar el directorio o carpeta donde se guardará el archivo que recibamos. Además, hay tres elecciones relevantes que podemos hacer. Deshabilitar el envío de archivo y/o texto, en caso de que solo queramos recibir una de estas dos cosas. También podemos elegir que el enlace sea de acceso público, para que no se solicite ninguna contraseña.

Una vez que marcamos nuestras opciones, presionamos **Iniciar Modo de Recepción**

![onionshare15](../../img/IMG-OnionShare/onionshare15.png)

OnionShare generará la url que debemos compartir para que nuestro interlocutor pueda enviarnos archivos o mensajes. Además, si no elegimos la opción de dejar el enlace con acceso público, debemos también copiar la contraseña asociada.

![onionshare27](../../img/IMG-OnionShare/onionshare27.png)

Cuando nuestro interlocutor reciba el enlace y lo agregue en su navegador Tor, verá la interfaz de OnionShare para compartir archivos y un recuadro para enviar mensajes. Para compartir archivos, debe seleccionarlos de la ubicación en su computador tras presionar la opción "**Browse**". Luego, habiendo subido los archivos que se quieran compartir y/o el mensaje que se quiere enviar, debe presionar "**Submit**".

![onionshare16](../../img/IMG-OnionShare/onionshare16.png)

Tras lo cual, aparecerá un mensaje indicando que el archivo y/o mensaje fue enviado.

![onionshare18](../../img/IMG-OnionShare/onionshare18.png)

En OnionShare, en cambio, en la pestaña donde recibiremos los contenidos, podremos ver el aviso de mensaje recibido y el archivo que podemos descargar. Además, también podemos detener la recepción de contenidos presionando "**Detener Modo de Recepción**".

![onionshare19](../../img/IMG-OnionShare/onionshare19.png) 
