# Descarga

Para descargar Onion Share debemos ir a [https://onionshare.org/#download](https://onionshare.org/#download) y elegir el sistema operativo que usamos. 

![onionshare01](../../img/IMG-OnionShare/onionshare01.png)

Al descargarse el archivo, hay que ejecutarlo y seguir las instrucciones de instalación de nuestro sistema. En GNU/Linux, la instalación se debe realizar por medio de [Snap o Flatpak](https://docs.onionshare.org/2.5/en/install.html#install-in-linux).

![onionshare02](../../img/IMG-OnionShare/onionshare02.png)
![onionshare03](../../img/IMG-OnionShare/onionshare03.png)
![onionshare04](../../img/IMG-OnionShare/onionshare04.png)

Una vez instalado, abrimos el programa. Si no sabemos dónde se encuentra, en el buscador del sistema lo ubicaremos por su nombre: OnionShare.

![onionshare05](../../img/IMG-OnionShare/onionshare05.png) 
