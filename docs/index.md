<p align="center">
     <img src="./Tor-onionshare-logo.png" width="500" />
</p>

# Sobre esta guía

Esta guía contiene una explicación general del Proyecto Tor y su red para una perspectiva anticapitalista, junto a una descripción detallada para la instalación y uso del navegador Tor y de OnionShare.

## Colectiviza y construye autonomía

Este contenido está elaborado por Colectivo Disonancia

<p align="left">
     <img src="./Logo_Disonancia.png" width="200" />
</p>

[Sitio Disonancia](https://colectivodisonancia.net){ .md-button } [:fontawesome-brands-gitlab: Repositorio GitLab](https://gitlab.com/cdisonancia){: .md-button }
