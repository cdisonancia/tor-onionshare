# Guía de navegación anónima con Tor y OnionShare

## Sobre esta guía

Esta guía contiene una explicación general del Proyecto Tor y su red para una perspectiva anticapitalista, junto a una descripción detallada para la instalación y uso del navegador Tor y de OnionShare.

## Acceso

La guía se encuentra dividida en dos partes en nuestro sitio: 
* Navegador Tor: [https://colectivodisonancia.net/tor/](https://colectivodisonancia.net/tor/)
* OnionShare: [https://colectivodisonancia.net/onionshare/](https://colectivodisonancia.net/onionshare/)

También disponible aquí: [https://cdisonancia.gitlab.io/tor-onionshare/](https://cdisonancia.gitlab.io/tor-onionshare/)


## Licencia

![CC BY-NC-SA](https://colectivodisonancia.net/wp-content/uploads/2020/02/CC-BY-NC-SA_90.jpg)

Esta guía, elaborada por **Colectivo Disonancia**, está bajo una licencia [Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es), exceptuando los contenidos que indiquen lo contrario.